﻿using System.Collections.Generic;
using System.Linq;
using Avico.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avico.Controllers
{
    /// <summary>
    /// Contacts Controllers
    /// </summary>
    [Route("api/contacts"), Produces("application/json")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private List<Contacts> contacts = new List<Contacts>() {
            new Contacts {
                Id = 1,
                Nom = "DUPUYS",
                Prenom = "Thomas",
                Adresses = new List<Adresse> {
                    new Adresse {
                        Id = 1,
                        NumeroRue = 58,
                        NomRue = "rue des lilats",
                        NomVille = "Paris",
                        CodePostal = 75000,
                        ContactId = 1
                    },
                    new Adresse {
                        Id = 2,
                        NumeroRue = 75,
                        NomRue = "rue de trois fleurs",
                        NomVille = "Paris",
                        CodePostal = 75000,
                        ContactId = 1
                    }
                },
                Telephones = new List<Telephone> {
                    new Telephone {
                        Id = 1,
                        NumeroTelephone = 0658239625,
                        ContactId = 1
                    },
                    new Telephone {
                        Id = 2,
                        NumeroTelephone = 0658639625,
                        ContactId = 1
                    }
                },
            },

            new Contacts {
                Id = 2,
                Nom = "DURANT",
                Prenom = "Charles",
                Adresses = null,
                Telephones = null
            },
        };

        /// <summary>
        /// Récupération de la liste des contacts.
        /// </summary>
        /// <returns>Liste des contacts</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Contacts>> GetContacts()
        {
            if (contacts.Count() == 0)
            {
                return NotFound();
            }

            return Ok(contacts);
        }

        /// <summary>
        /// Récupération d'un contact spécifique en fonction de son identifiant
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <returns>Contact spécifique</returns>
        [HttpGet("{idContact}"), Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Contacts> GetContactById(int idContact)
        {
            var contact = contacts.Where(c => c.Id == idContact).FirstOrDefault();
            if (contact is null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        /// <summary>
        /// Récupération de toutes les adresses d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <returns>Liste des adresses d'un contact</returns>
        [HttpGet("{idContact}/adresses"), Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Adresse>> GetAdressesByContactId(int idContact)
        {
            var adresses = contacts.Where(c => c.Id == idContact).FirstOrDefault().Adresses;
            if (adresses is null)
            {
                return NotFound();
            }

            return Ok(adresses);
        }


        /// <summary>
        /// Récupération de toutes les numéros de téléphone d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <returns>Liste des numéros de téléphone d'un contact</returns>
        [HttpGet("{idContact}/telephones"), Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Telephone>> GetTelephonesByContactId(int idContact)
        {
            var telephones = contacts.Where(c => c.Id == idContact).FirstOrDefault().Telephones;
            if (telephones is null)
            {
                return NotFound();
            }

            return Ok(telephones);
        }

        /// <summary>
        /// Modification d'un contact
        /// </summary>
        /// /// <param name="idContact">Identifiant du contact</param>
        /// <param name="contactDTO">Contact</param>
        /// <returns>Contact modifié</returns>
        [HttpPut("{idContact}"), Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Contacts> Put(int idContact, [FromBody] Contacts contactDTO)
        {
            if (idContact != contactDTO.Id)
            {
                return BadRequest();
            }

            var contactInList = contacts.Where(c => c.Id == idContact).FirstOrDefault();
            if (contactInList is null)
            {
                return NotFound();
            }

            contactInList = contactDTO;

            return Ok(contactInList);
        }

        /// <summary>
        /// Modification d'une adresse d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <param name="idAdresse">Identifiant de l'adresse</param>
        /// <param name="adresseDTO">Adresse</param>
        /// <returns>Adresse modifiée</returns>
        [HttpPut("{idContact}/adresses/{idAdresse}"), Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Adresse> PutAdresse(int idContact, int idAdresse, [FromBody] Adresse adresseDTO)
        {
            if (idAdresse != adresseDTO.Id)
            {
                return BadRequest();
            }

            var contact = contacts.Where(c => c.Id == idContact).FirstOrDefault();

            if (contact is null)
            {
                return NotFound();
            }

            var adresseInList = contact.Adresses.Where(a => a.Id == idAdresse).FirstOrDefault();
            if (adresseInList is null)
            {
                return NotFound();
            }

            adresseInList = adresseDTO;

            return Ok(adresseInList);
        }

        /// <summary>
        /// Modification d'un numéro de téléphone d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <param name="idTelephone">Identifiant du numéro de téléphone</param>
        /// <param name="telephoneDTO">Telephone</param>
        /// <returns>Numéro de téléphone modifiée</returns>
        [HttpPut("{idContact}/telephones/{idTelephone}"), Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Telephone> PutTelephone(int idContact, int idTelephone, [FromBody] Telephone telephoneDTO)
        {
            if (idTelephone != telephoneDTO.Id)
            {
                return BadRequest();
            }

            var contact = contacts.Where(c => c.Id == idContact).FirstOrDefault();

            if (contact is null)
            {
                return NotFound();
            }

            var telephoneInList = contact.Telephones.Where(a => a.Id == idTelephone).FirstOrDefault();
            if (telephoneInList is null)
            {
                return NotFound();
            }

            telephoneInList = telephoneDTO;

            return Ok(telephoneInList);
        }

        /// <summary>
        /// Supression d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        [HttpDelete("{idContact}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete(int idContact)
        {
            var contact = contacts.Where(c => c.Id == idContact).FirstOrDefault();
            if (contact is null)
            {
                return NotFound();
            }

            contacts.Remove(contact);

            return NoContent();
        }

        /// <summary>
        /// Supression d'une adresse d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <param name="idAdresse">Identifiant de l'adresse</param>
        [HttpDelete("{idContact}/adresses/{idAdresse}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteAdresse(int idContact, int idAdresse)
        {
            var contact = contacts.Where(c => c.Id == idContact).FirstOrDefault();
            if (contact is null)
            {
                return NotFound();
            }

            var adresse = contact.Adresses.Where(a => a.Id == idAdresse).FirstOrDefault();
            if (adresse is null)
            {
                return NotFound();
            }

            contact.Adresses.Remove(adresse);

            return NoContent();
        }

        /// <summary>
        /// Supression d'un numéro de téléphone d'un contact
        /// </summary>
        /// <param name="idContact">Identifiant du contact</param>
        /// <param name="idTelephone">Identifiant du numéro de téléphone</param>
        [HttpDelete("{idContact}/telephone/{idTelephone}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteTelephone(int idContact, int idTelephone)
        {
            var contact = contacts.Where(c => c.Id == idContact).FirstOrDefault();
            if (contact is null)
            {
                return NotFound();
            }

            var telephone = contact.Telephones.Where(a => a.Id == idTelephone).FirstOrDefault();
            if (telephone is null)
            {
                return NotFound();
            }

            contact.Telephones.Remove(telephone);

            return NoContent();
        }
    }
}
