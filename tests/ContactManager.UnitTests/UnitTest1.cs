using System.Net.Mail;
using System;
using Xunit;
using FluentAssertions;
using Avico.DTO;

namespace ContactManager.UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void TestExistanceAdresse()
        {

            Adresse MyAddress = new Adresse
            {
                Id = 1,
                NumeroRue = 58,
                NomRue = "rue des lilats",
                NomVille = "Paris",
                CodePostal = 75000,
                ContactId = 1
            };

            MyAddress.Id.Should().Be(1);
            MyAddress.NumeroRue.Should().Be(58);
            MyAddress.NomRue.Should().Be("rue des lilats");
            MyAddress.NomVille.Should().Be("Paris");
            MyAddress.CodePostal.Should().Be(75000);
            MyAddress.ContactId.Should().Be(1);
        }
    }
}
