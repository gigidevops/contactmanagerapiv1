using System.Security.Cryptography.X509Certificates;
using System.Net.Http.Headers;
using System.Net;
using System;
using System.Net.Http;
using System.Data.Common;
using System.Threading.Tasks;
using AspNetCore.Http.Extensions;
using Avico;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
using FluentAssertions;
using Avico.DTO;


namespace ContactManager.FunctionnalTests
{
    public class UnitTest1
    {
        private readonly TestServer _testServer;
       private readonly HttpClient _testClient;
       public UnitTest1()
       {
           //Initializing the test environment
           _testServer = new TestServer(new WebHostBuilder()
               .UseStartup<Startup>());
           //Test client creation
           _testClient = _testServer.CreateClient();
       }

       [Fact]
       public async Task TestGetRequestAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts");
           //response.EnsureSuccessStatusCode();

           response.StatusCode.Should().Be(HttpStatusCode.OK);
       }

       [Fact]
       public async Task TestGetContactsList()
       {
           var response = await _testClient.GetAsync("/api/contacts/0");

           response.StatusCode.Should().Be(HttpStatusCode.NotFound);
       }
       
    }
}
